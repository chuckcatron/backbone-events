$(function() {
	var vent = _.extend({}, Backbone.Events);

	App.currentUser = new App.Models.User();
	
	var homeView = new App.Views.Home({el: $('#homeView'), model: App.currentUser, vent: vent});
	var aboutView = new App.Views.About({el: $('#aboutView'), model: App.currentUser, vent: vent});
	var menuView = new App.Views.Menu({el: $('#menuView'), model: App.currentUser, vent: vent});
	var contactView = new App.Views.Contact({el: $('#contactView'), model: App.currentUser, vent: vent});

	homeView.render(false);
	aboutView.render(true);
	contactView.render(true);
});