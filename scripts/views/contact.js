App.Views.Contact = Backbone.View.extend({

	events: {},

	initialize: function(options) {
		_.bindAll(this, "showcontact");
    	options.vent.bind("showcontact", this.showcontact);
		this.vent = options.vent;
		hide = false;
    },

	render: function(hide) {
		this.$el.toggleClass("hidden", hide);
		return this;
	},

	showcontact: function(){
    	this.render(false);
    }
});