App.Views.Menu = Backbone.View.extend({

	events: {
		"click #btnNavHome": "home",
		"click #btnNavAbout": "about",
		"click #btnNavContact": "contact",
	},

	initialize: function(options){
		this.render();
		this.vent = options.vent;
	},

	render: function(){
		return this;
	},

	home: function(){
		$("#contactView").toggleClass("hidden", true);
		$("#aboutView").toggleClass("hidden", true);
		this.vent.trigger("showHome", this.model);
		this.updateDisplay($("#btnNavHome"));
		toastr.success("home event fired");
	},

	about: function(){
		$("#contactView").toggleClass("hidden", true);
		$("#homeView").toggleClass("hidden", true);
		this.vent.trigger("showAbout", this.model);
		this.updateDisplay($("#btnNavAbout"));
		toastr.success("about event fired");
	},

    contact: function(){
    	$("#homeView").toggleClass("hidden", true);
    	$("#aboutView").toggleClass("hidden", true);
    	this.vent.trigger("showcontact", this.model);
    	this.updateDisplay($("#btnNavContact"));	
    	toastr.success("contact event fired");
    },

    updateDisplay: function(btn){
    	$('#navUL').children().removeClass("active");
  		$(btn).parent().addClass('active');
    }
});